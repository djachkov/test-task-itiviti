import json # Импорт модуля для работы с json
import os
from flask import Flask, request, jsonify, abort, request # Используемые модули фласка
from configuration import Configuration # Выносим конфигурацию фласк в отдельный файл

# Создание приложения и подключение файла конфигурации
app = Flask(__name__)
app.config.from_object(Configuration)
# Создаем путь
log_path = os.getcwd() + '/log'
if not os.path.exists(log_path):
    os.makedirs(log_path)
# Проверить есть ли файл с логами, если нет - создание
try:
    with open('log/requests.json', 'r') as f:
        data = json.load(f)
except:
    with open('log/requests.json', 'w+') as f:
        json.dump([], f)

# Статический контент по корневому пути
@app.route('/', methods=['GET'])
def home():
    return "<h1>Test API</h1><p>This is the testing API</p>"

# Показать все запросы
@app.route('/api/v1/data', methods=['GET'])
def get_requests():
    return jsonify(data)

# Отправка запроса и добавление в архив + сохранение отдельным файлом
@app.route('/api/v1/data/add', methods=['POST'])
def set_request():
    if not request.json or not 'id' in request.json: # Отсеиваем запросы с неправильным форматом или без id
        abort(400)
    else:
        for item in data:
            if request.json['id'] == item['id']: # Отсеиваем запросы с дублирующимися id
                abort(400)
    task = request.json
    data.append(task)
    file_id = request.json['id']
    keylist = list(request.json)
    try:
        filename = keylist[1]
    except:
        filename = "NONAME"
    # Сохраняем запрос в общий архив и отдельным json
    with open('log/requests.json', 'w') as f:
        json.dump(data, f)
    with open('log/request_id' + file_id + '_' + filename + '.json', 'w') as f:
        json.dump(request.json, f)
    return jsonify(task), 201
print('This is the testing message')
